﻿using System;
using System.Diagnostics;
using System.Net;
using SimpleHttp;
using System.Threading;

namespace UWPStartServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Process proc = new Process { EnableRaisingEvents = true };
            proc.Exited += Proc_Exited;
            
            Route.Add("/startUWPApp/{app}/{args}", (req, res, props) =>
            {
                res.AsText("App wiil be started.");
                try
                {
                    proc.StartInfo.FileName = WebUtility.HtmlDecode(props["app"]).Replace("/", "\\").Replace("%20", " ");
                    proc.StartInfo.Arguments = WebUtility.HtmlDecode(props["args"]).Replace("/","\\").Replace("%20", " ");
                    proc.Start();
                }
                catch
                {

                }
                
            });
            HttpServer.ListenAsync(9876, CancellationToken.None, Route.OnHttpRequestAsync).Wait();
            Console.ReadLine();
        }

        private static void Proc_Exited(object sender, EventArgs e)
        {
            Console.WriteLine("App is closed");
        }
    }
}
